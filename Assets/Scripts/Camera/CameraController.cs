﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour {
	public GameObject plane;
	public float planeCoor;
	private float _cameraCoorY, _cameraCoorZ;
	// Use this for initialization
	void Start () {
		_cameraCoorY = transform.position.y;
		_cameraCoorZ = transform.position.z;
	}
	
	// Update is called once per frame
	void Update () {
		planeCoor = plane.GetComponent<Transform>().transform.position.x;

		if (planeCoor < 240 && planeCoor > -240) {
			transform.position = new Vector3(planeCoor, _cameraCoorY, _cameraCoorZ);
		}
	}
}
