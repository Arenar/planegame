﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstaclePrefabController : MonoBehaviour {
	private GameObject _spawnTarget;
	// Use this for initialization
	void Start () {
		_spawnTarget = GameObject.Find ("SpawnTarget");
	}
	
	// Update is called once per frame
	void Update () {
		ObstaclePrefabSpeed();
	}

	private void ObstaclePrefabSpeed(){
		transform.Translate(0, 0, (-1) * _spawnTarget.gameObject.GetComponent<ObstacleSpeedUpdater>().obstaclesSpeed);
	}
}
