﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleSphereMovement : MonoBehaviour {
	private int _turn;
	private float _sphereSpeed = 50.0f;
	// Use this for initialization
	void Start () {
		_turn = Random.Range (0, 1);	//на заметку, погуглить, почему не пашет Convert.ToBoolean
	}

	// Update is called once per frame
	void Update () {
		if (_turn == 0){
			transform.Translate(_sphereSpeed * Time.deltaTime, 0, 0);
		} else if (_turn == 1) {
			transform.Translate((-1) * _sphereSpeed * Time.deltaTime, 0, 0);
		}
	}

	private void OnTriggerEnter(Collider wall){
		switch (wall.gameObject.name) {
		case "LeftWall":
			_turn = 0;
			break;
		case "RightWall":
			_turn = 1;
			break;
		}
	}
}
