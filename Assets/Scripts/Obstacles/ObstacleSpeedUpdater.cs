﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleSpeedUpdater : MonoBehaviour {
	public float obstaclesSpeed;
	public float obstaclesSpeedBoost;
	private float _obstaclesSpeedMax;
	// Use this for initialization
	void Start () {
		obstaclesSpeed = 1.0f;
		obstaclesSpeedBoost = 0.1f;
		_obstaclesSpeedMax = 25.0f;
	}
	
	// Update is called once per frame
	void Update () {
		if (obstaclesSpeed <= _obstaclesSpeedMax) {
			obstaclesSpeed += obstaclesSpeedBoost * Time.deltaTime;
		}
	}
}
