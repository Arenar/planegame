﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeadZoneController : MonoBehaviour {
	public GameObject spawnTarget;
	// Use this for initialization
	void Start () {
		spawnTarget = GameObject.Find ("SpawnTarget");
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnTriggerEnter(Collider obstaclePrefab) {
		if (obstaclePrefab.gameObject.tag == "ObstaclePrefab") {
			Debug.Log ("ObstaclePrefabDestroy");
			spawnTarget.gameObject.GetComponent<ObstaclePrefabSpawnController> ().isExist = false;
			Destroy (obstaclePrefab.gameObject);
		}
	}
}
