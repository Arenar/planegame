﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RingController : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		transform.eulerAngles = new Vector3 (0,0, 15.0f);
	}

	void OnTriggerEnter(Collider otherObj) {
		if (otherObj.gameObject.tag == "Player") {
			otherObj.gameObject.GetComponent<PlaneController>().score += 1;
		}
	}
}
