﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstaclePrefabSpawnController : MonoBehaviour {
	public GameObject[] spawnObjects;

	public GameObject spawnPrefabOne;
	public GameObject spawnPrefabTwo;
	public GameObject spawnPrefabThree;
	public GameObject spawnPrefabFour;

	public bool isExist;
	// Use this for initialization
	void Start () {
		spawnObjects = new GameObject[4];

		spawnObjects [0] = spawnPrefabOne;
		spawnObjects [1] = spawnPrefabTwo;
		spawnObjects [2] = spawnPrefabThree;
		spawnObjects [3] = spawnPrefabFour;

		isExist = false;
	}
	
	// Update is called once per frame
	void Update () {
		if (!isExist) {
			Instantiate(spawnObjects[Random.Range(0, spawnObjects.Length)], new Vector3 (0,0,450), Quaternion.identity);
			isExist = !isExist;
		}
	}
}
