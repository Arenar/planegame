﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleController : MonoBehaviour {
	void OnTriggerEnter(Collider otherObj) {
		if (otherObj.gameObject.tag == "Player") {
			otherObj.gameObject.GetComponent<PlaneController>().PlaneCrush();
		}
	}
}