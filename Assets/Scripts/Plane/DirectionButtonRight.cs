﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.EventSystems;
using UnityEngine.UI;

public class DirectionButtonRight : MonoBehaviour, IPointerDownHandler, IPointerUpHandler {
	public GameObject plane;
	public bool onPressedRight;

	private float _planePosition;
	// Use this for initialization
	void Start () {
		onPressedRight = false;
	}

	// Update is called once per frame
	void Update () {
		if (onPressedRight) {
			plane.GetComponent<PlaneController>().PlaneTurnRight();
		}
	}
	public void OnPointerDown(PointerEventData eventData){
		onPressedRight = true;
	}

	public void OnPointerUp(PointerEventData eventData){
		onPressedRight = false;
	}


}
