﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class DirectionButtonLeft : MonoBehaviour, IPointerDownHandler, IPointerUpHandler {
	public GameObject plane;
	public bool onPressedLeft;
	// Use this for initialization
	void Start () {
		onPressedLeft = false;

	}

	// Update is called once per frame
	void Update () {
		if (onPressedLeft) {
			plane.GetComponent<PlaneController>().PlaneTurnLeft();
		} 
	}

	public void OnPointerDown(PointerEventData eventData){
		onPressedLeft = true;
	}

	public void OnPointerUp(PointerEventData eventData){
		onPressedLeft = false;
	}
}
