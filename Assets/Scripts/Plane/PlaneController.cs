﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlaneController : MonoBehaviour {
	public GameObject buttonLeft;
	public GameObject buttonRight;
	public GameObject panelController;
	public GameObject panelGameOver;

	private float _planePosition = 0.0f;

	private float _turnSpeed = 0.0f;
	private float _turnSpeedBoost = 5.0f;
	private float _turnSpeedMax = 10.0f;

	private float _rotateAngle = 0.0f;
	private float _rotateSpeedBoost = 80.0f;
	private float _angleOfSlopeLeftMax = 65.0f;
	private float _angleOfSlopeRightMax = -65.0f;

	public bool isAlive;
	private float _planeFall;
	public int score;

	// Use this for initialization
	void Start () {
		isAlive = true;
		score = 0;
		buttonLeft = GameObject.Find ("ButtonLeft");
		buttonRight = GameObject.Find ("ButtonRight");
		panelController = GameObject.Find("PanelController");
	}
	
	// Update is called once per frame
	void Update () {
		_planePosition = transform.position.x;
		PlaneStop ();

		if (isAlive == false) {
			panelController.SetActive(false);
			panelGameOver.SetActive(true);
			GetComponent<Rigidbody>().useGravity = true;
			_planeFall -= 15 * Time.deltaTime;
			transform.eulerAngles = new Vector3 (0,0, _planeFall);
		}
	}

	public void PlaneTurnLeft(){
		if (_turnSpeed <= _turnSpeedMax) {
			_turnSpeed += _turnSpeedBoost * Time.deltaTime;
			transform.position = new Vector3 (_planePosition - _turnSpeed, 0, 0);
		}
		if (_rotateAngle <= _angleOfSlopeLeftMax) {
			_rotateAngle += _rotateSpeedBoost * Time.deltaTime; 
			transform.eulerAngles = new Vector3 (0,0,_rotateAngle);
		}
	}

	public void PlaneTurnRight(){
		if (_turnSpeed <= _turnSpeedMax) {
			_turnSpeed -= _turnSpeedBoost * Time.deltaTime;
			transform.position = new Vector3 (_planePosition - _turnSpeed, 0, 0);
		}
		if (_rotateAngle >= _angleOfSlopeRightMax) {
			_rotateAngle -= _rotateSpeedBoost * Time.deltaTime; 
			transform.eulerAngles = new Vector3 (0,0,_rotateAngle);
		}
	}

	public void PlaneStop(){
		if (!buttonLeft.GetComponent<DirectionButtonLeft>().onPressedLeft && 
			!buttonRight.GetComponent<DirectionButtonRight>().onPressedRight) {
			if (_turnSpeed >= 0.0f) {
				_turnSpeed -= 2 * _turnSpeedBoost * Time.deltaTime;
			} else if (_turnSpeed <= 0.0f) {
				_turnSpeed += 2 * _turnSpeedBoost * Time.deltaTime;
			}

			if (_rotateAngle > 0.0f) {
				_rotateAngle -= _rotateSpeedBoost * Time.deltaTime; 
				if (_rotateAngle < 0.0f) {
					_rotateAngle = 0;
				}
			} else if (_rotateAngle < 0.0f) {
				_rotateAngle += _rotateSpeedBoost * Time.deltaTime; 
				if (_rotateAngle > 0.0f) {
					_rotateAngle = 0;
				}
			}
			transform.eulerAngles = new Vector3 (0,0,_rotateAngle);
		}
	}

	public void PlaneCrush(){
		isAlive = false;
	}
}
