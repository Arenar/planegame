﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PauseMenuController : MonoBehaviour {
	public GameObject panelPauseMenu;
	public GameObject panelController;

	public void doQuit () {
		Application.Quit();
	}

	public void doRestartLevel(){
		SceneManager.LoadScene("game_level");
	}

	public void doLoadMainMenu(){
		SceneManager.LoadScene("main_menu");
	}
		
	public void doContinue(){
		Time.timeScale = 1;
		panelPauseMenu.SetActive(false);
		panelController.SetActive(true);
	}
}
