﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameplayButtonController : MonoBehaviour {
	public GameObject panelPauseMenu;
	public GameObject panelController;

	public void doGamePause(){
		Time.timeScale = 0;
		panelPauseMenu.SetActive(true);
		panelController.SetActive(false);
	}
}
