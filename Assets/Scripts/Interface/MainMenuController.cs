﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MainMenuController : MonoBehaviour {

	public void doLoadGame(){
		SceneManager.LoadScene("game_level");
	}

	public void doQuit () {
		Application.Quit();
	}
}
